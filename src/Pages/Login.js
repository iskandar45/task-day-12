import { Box, Button, TextField, Typography } from "@mui/material";
import React, { useContext, useState } from "react";
import { AuthContext } from "../Contexts";
import { useHistory } from "react-router-dom";

export const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login, isLoggedIn } = useContext(AuthContext);
  const [error, setError] = useState(false);

  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (login(username, password)) {
      history.push("/");
      return;
    }
    setError(true);
  };

  return (
    <Box
      marginTop={10}
      marginInline="auto"
      display="flex"
      flexDirection="column"
      gap={3}
      sx={{
        width: 500,
        bgcolor: "white",
        padding: "20px",
        borderRadius: "10px",
      }}
    >
      {error && (
        <Typography variant="caption" color="red">
          Username / password salah
        </Typography>
      )}

      <TextField
        type="text"
        label="Username"
        id="username"
        onChange={(e) => setUsername(e.target.value)}
      />
      <TextField
        type="password"
        label="Password"
        id="password"
        onChange={(e) => setPassword(e.target.value)}
      />
      <Button variant="contained" onClick={handleSubmit}>
        Login
      </Button>
      <Typography variant="caption">
        Username: admin / Password: admin123
      </Typography>
    </Box>
  );
};
