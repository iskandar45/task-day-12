import { Box, Card, Container, Divider, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { CommonContext } from "../Contexts";

export const About = () => {
  const appBar = React.useContext(CommonContext);

  useEffect(() => {
    appBar.changeData("About Page", "#8294C4");
  }, []);

  return (
    <Container
      sx={{
        gap: "20px",
        padding: "20px",
      }}
    >
      <Card style={{ padding: "20px" }}>
        <Typography gutterBottom variant="h4" component="div">
          About Us
        </Typography>
        <Divider sx={{ marginBottom: "10px" }} />
        <Typography variant="body2" color="text.secondary">
          Morbi tristique senectus et netus et. Mattis ullamcorper velit sed
          ullamcorper morbi tincidunt. Vitae nunc sed velit dignissim sodales ut
          eu. Eget nullam non nisi est. Quam viverra orci sagittis eu. Faucibus
          scelerisque eleifend donec pretium vulputate sapien nec sagittis
          aliquam. At imperdiet dui accumsan sit amet nulla. Velit laoreet id
          donec ultrices tincidunt arcu. Dolor sit amet consectetur adipiscing
          elit pellentesque habitant morbi tristique. In fermentum posuere urna
          nec tincidunt praesent. Faucibus et molestie ac feugiat sed. Accumsan
          in nisl nisi scelerisque eu ultrices vitae.
        </Typography>
        <br />
        <Typography variant="body2" color="text.secondary">
          Tristique et egestas quis ipsum suspendisse. Sed cras ornare arcu dui
          vivamus arcu felis bibendum. Eu mi bibendum neque egestas. Eleifend
          donec pretium vulputate sapien nec sagittis aliquam malesuada
          bibendum. Mauris cursus mattis molestie a iaculis. Egestas sed sed
          risus pretium quam vulputate dignissim suspendisse. Ultrices gravida
          dictum fusce ut placerat orci nulla. Fames ac turpis egestas maecenas
          pharetra convallis posuere morbi leo. Sit amet purus gravida quis
          blandit turpis. Morbi tristique senectus et netus. Quisque id diam vel
          quam elementum pulvinar etiam non quam.
        </Typography>
      </Card>
    </Container>
  );
};
