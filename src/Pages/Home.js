import React, { useContext, useEffect } from "react";
import { Layout } from "../Components/CommonProvider";
import { Box, Card, Container, Divider, Typography } from "@mui/material";
import { AuthContext, CommonContext } from "../Contexts";

export const Home = () => {
  const { user } = useContext(AuthContext);
  const appBar = useContext(CommonContext);
  useEffect(() => {
    appBar.changeData("Home Page", "#B04759");
  }, []);
  return (
    <Container
      style={{
        gap: "20px",
        padding: "20px",
      }}
    >
      <Card style={{ padding: "20px" }}>
        <Typography gutterBottom variant="h4" component="div">
          Welcome, {user.name}
        </Typography>
        <Divider sx={{ marginBottom: "10px" }} />
        <Typography variant="body2" align="justify" color="text.secondary">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Nisi
          scelerisque eu ultrices vitae auctor. Erat pellentesque adipiscing
          commodo elit at imperdiet dui accumsan. Volutpat lacus laoreet non
          curabitur gravida arcu ac. In hac habitasse platea dictumst quisque
          sagittis purus. Vitae aliquet nec ullamcorper sit. Tincidunt dui ut
          ornare lectus sit amet. Elementum eu facilisis sed odio. Elit ut
          aliquam purus sit amet luctus venenatis lectus. Bibendum ut tristique
          et egestas quis ipsum suspendisse. Purus non enim praesent elementum
          facilisis. Leo integer malesuada nunc vel. Nam libero justo laoreet
          sit amet cursus.
        </Typography>
      </Card>
    </Container>
  );
};
