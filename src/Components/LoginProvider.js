import React, { useState } from "react";
import { AuthContext } from "../Contexts";

const accountUser = [
  {
    id: 1,
    name: "Admin",
    username: "admin",
    password: "admin123",
  },
];

export const LoginProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const login = (username, password) => {
    const user = accountUser.find(
      (user) => user.username === username && user.password === password
    );

    if (user) {
      setUser(user);
      setIsLoggedIn(true);
      return true;
    }

    setIsLoggedIn(false);
    return false;
  };
  return (
    <AuthContext.Provider value={{ user, login, isLoggedIn }}>
      {children}
    </AuthContext.Provider>
  );
};
