import { useState } from "react";
import { CommonContext } from "../Contexts";
import { CustomAppBar } from "./CustomAppBar";

export const CommonProvider = ({ children }) => {
  const [appBarData, setAppBarData] = useState({
    title: "My App",
    color: "#004aad",
  });

  const changeData = (title, color) => {
    setAppBarData({ ...appBarData, title, color });
  };
  return (
    <CommonContext.Provider value={{ ...appBarData, changeData }}>
      <CustomAppBar
        noButton={true}
        title={appBarData.title}
        color={appBarData.color}
      />
      {children}
    </CommonContext.Provider>
  );
};
